package com.orion.smart.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.Cursor;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ScanOptions;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="http://github.com/athc">dujf</a>
 * @date 2018/9/10
 * @since JDK1.8
 */
@RestController
@RequestMapping("/redis/set")
public class RedisSetController {


  private final RedisTemplate redisTemplate;

  @Autowired
  public RedisSetController(RedisTemplate redisTemplate) {
    this.redisTemplate = redisTemplate;
  }

  /**
   * redis 操作list
   */
  @PostMapping
  public void setSet() {
    //添加
    redisTemplate.opsForSet().add("name", "zhaowu", "lissi", "xhjahng", "wahhah");

  }

  /**
   * redis 操作list
   */
  @GetMapping
  public void getSet() {
    Object result = null;

    //移动一个set 中的 value 到 另一个set
    result = redisTemplate.opsForSet().move("name", "zhangsan", "name");
    logger.info(String.valueOf(result));

    //判段是否是成员
    result = redisTemplate.opsForSet().isMember("name", "zhangsan");
    logger.info(String.valueOf(result));

    //求两个集合交集
    result = redisTemplate.opsForSet().intersect("name", "name");
    logger.info(String.valueOf(result));

    //求两个集合交集 存在另一个集合中
    result = redisTemplate.opsForSet().intersectAndStore("name", "name", "name");
    logger.info(String.valueOf(result));

    //求两个集合并集
    result = redisTemplate.opsForSet().union("name", "name");
    logger.info(String.valueOf(result));

    //求两个集合并集 存在另一个集合中
    result = redisTemplate.opsForSet().unionAndStore("name", "name", "name");
    logger.info(String.valueOf(result));

    //求两个集合差集
    result = redisTemplate.opsForSet().difference("name", "name");
    logger.info(String.valueOf(result));

    //求两个集合差集 存在另一个集合中
    result = redisTemplate.opsForSet().differenceAndStore("name", "name", "name");
    logger.info(String.valueOf(result));

    //redis set 大小
    result = redisTemplate.opsForSet().size("name");
    logger.info(String.valueOf(result));

    //集合所有成员
    result = redisTemplate.opsForSet().members("name");
    logger.info(String.valueOf(result));

    //随机获取集合中的一个元素
    result = redisTemplate.opsForSet().randomMember("name");
    logger.info(String.valueOf(result));

    //获取多个key无序集合中的元素（去重），count表示个数
    result = redisTemplate.opsForSet().distinctRandomMembers("name", 3);
    logger.info(String.valueOf(result));

    //遍历集合
    Cursor cursor = redisTemplate.opsForSet().scan("name", ScanOptions.NONE);
    while (cursor.hasNext()) {
      result = cursor.next();
      logger.info(String.valueOf(result));
    }
  }

  /**
   * 删除list 中的值
   */
  @DeleteMapping
  public void remove() {
    //移除一个或多个元素
    redisTemplate.opsForSet().remove("name", "zhaowu", "lissi");
    //移除并返回集合中的一个随机元素
    redisTemplate.opsForSet().pop("name");
  }


  private Logger logger = LoggerFactory.getLogger(RedisListController.class);
}

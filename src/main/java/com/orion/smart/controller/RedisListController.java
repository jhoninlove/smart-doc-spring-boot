package com.orion.smart.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author <a href="http://github.com/athc">dujf</a>
 * @date 2018/9/10
 * @since JDK1.8
 */
@RestController
@RequestMapping("/redis/list")
public class RedisListController {

  private final RedisTemplate redisTemplate;

  @Autowired
  public RedisListController(RedisTemplate redisTemplate) {
    this.redisTemplate = redisTemplate;
  }

  /**
   * redis 操作list
   */
  @PostMapping
  public void setList() {
    //在最左边加入
    redisTemplate.opsForList().leftPush("name", "zhaowu");
    //在最左边加入
    redisTemplate.opsForList().rightPush("name", "qianliu");
    //添加多个值
    List list = new ArrayList<>();
    list.add("anana");
    list.add("bbana");
    redisTemplate.opsForList().rightPush("name", list);
    //set value
    redisTemplate.opsForList().set("name", 0, "zhangsan");
    redisTemplate.opsForList().set("name", 1, "setValue");
    redisTemplate.opsForList().set("name", 2, "wangxier");
  }

  /**
   * redis 操作list
   */
  @GetMapping
  public void getList() {
    Object result = null;

    //返回存储在键中的列表的指定元素
    result = redisTemplate.opsForList().range("name", 0, -1);
    logger.info(String.valueOf(result));

    //删除范围外的元素
    redisTemplate.opsForList().trim("name", 1, -1);
    result = redisTemplate.opsForList().range("name", 0, -1);
    logger.info(String.valueOf(result));

    //返回list 大小
    result = redisTemplate.opsForList().size("name");
    logger.info(String.valueOf(result));

    //根据下标获取值
    result = redisTemplate.opsForList().index("name", 2);
    logger.info(String.valueOf(result));

    //取出并删除最左边元素
    result = redisTemplate.opsForList().leftPop("name");
    logger.info(String.valueOf(result));

    //取出并删除最右边元素
    result = redisTemplate.opsForList().rightPop("name");
    logger.info(String.valueOf(result));

    //将最右元素取出来放在最左
    result = redisTemplate.opsForList().rightPopAndLeftPush("name", "name");
    logger.info(String.valueOf(result));
  }

  /**
   * 删除list 中的值
   */
  @DeleteMapping
  public void remove() {
    //从存储在键中的列表中删除等于值的元素的第一个计数事件。
    //计数参数以下列方式影响操作：
    //count> 0：删除等于从头到尾移动的值的元素。
    //count <0：删除等于从尾到头移动的值的元素。
    //count = 0：删除等于value的所有元素。
    redisTemplate.opsForList().remove("name", 5, "zhangsan");
  }


  private Logger logger = LoggerFactory.getLogger(RedisListController.class);
}

package com.orion.smart.controller.graphql;

import graphql.schema.GraphQLEnumType;
import graphql.schema.GraphQLObjectType;
import graphql.schema.GraphQLUnionType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static graphql.Scalars.GraphQLBigDecimal;
import static graphql.Scalars.GraphQLBoolean;
import static graphql.Scalars.GraphQLInt;
import static graphql.Scalars.GraphQLString;
import static graphql.schema.GraphQLEnumType.newEnum;
import static graphql.schema.GraphQLFieldDefinition.newFieldDefinition;
import static graphql.schema.GraphQLObjectType.newObject;
import static graphql.schema.GraphQLUnionType.newUnionType;

/**
 * @author <a href="http://github.com/athc">dujf</a>
 * @date 2018/9/3
 * @since JDK1.8
 */
@RestController
@RequestMapping("grap")
public class GrapController {

  @GetMapping()
  public GraphQLObjectType hello() {
    return newObject()
        .name("Foo")
        .field(newFieldDefinition()
            .name("bar")
            .type(GraphQLString))
        .build();
  }

  /**
   * @return
   */
  @GetMapping("/simple")
  public GraphQLObjectType simple() {
    return newObject()
        .name("SimpsonCharacter")
        .description("A Simpson character")
        .field(newFieldDefinition()
            .name("name")
            .description("The name of the character.")
            .type(GraphQLString))
        .field(newFieldDefinition()
            .name("mainCharacter")
            .description("One of the main Simpson characters?")
            .type(GraphQLBoolean))
        .field(newFieldDefinition()
            .name("age")
            .description("age of people can't more than 200 or less than 0")
            .type(GraphQLInt)
            .build()
        )
        .build();
  }


  /**
   * @return
   */
  @GetMapping("/union")
  public GraphQLUnionType union() {
    return newUnionType()
        .name("Pet")
        .possibleType(
            GraphQLObjectType
                .newObject()
                .name("name")
                .description("")
                .field(newFieldDefinition().name("name").type(GraphQLString).build())
                .field(newFieldDefinition().name("pwd").type(GraphQLString).build())
                .build()
        )
        .possibleType(
            GraphQLObjectType
                .newObject()
                .name("name")
                .description("")
                .field(newFieldDefinition().name("age").type(GraphQLString).build())
                .field(newFieldDefinition().name("amount").type(GraphQLBigDecimal).build())
                .build()
        ).build();
  }

  /**
   * @return
   */
  @GetMapping("/enum")
  public GraphQLEnumType emmu() {
    return newEnum()
        .name("Color")
        .description("Supported colors.")
        .value("RED")
        .value("GREEN")
        .value("BLUE")
        .build();
  }
}


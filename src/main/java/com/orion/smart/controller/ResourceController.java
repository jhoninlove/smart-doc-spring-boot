package com.orion.smart.controller;

import com.orion.smart.service.ResourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * 资源
 *
 * @author <a href="http://github.com/athc">dujf</a>
 * @date 2018/8/31
 * @since JDK1.8
 */
@RestController
@RequestMapping("/re")
public class ResourceController {

  @Autowired
  private ResourceService resourceService;

  /**
   * 创建
   *
   * @param name 用户名
   * @return
   */
  @PostMapping
  public String create(String name) {
    resourceService.setKey(name, name);
    return name;
  }

  /**
   * 查询
   *
   * @param name
   * @return
   */
  @GetMapping
  public String getName(@RequestParam String name) {
    return (String) resourceService.getKey(name);
  }

  /**
   * 修改
   *
   * @param name
   * @return
   */
  @PutMapping
  public String update(String name) {
    return name;
  }

  /**
   * 删除
   *
   * @param name
   * @return
   */
  @DeleteMapping
  public String delete(String name) {
    return name;
  }
}

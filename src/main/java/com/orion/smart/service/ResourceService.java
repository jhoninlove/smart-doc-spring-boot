package com.orion.smart.service;

/**
 * @author <a href="http://github.com/athc">dujf</a>
 * @date 2018/9/10
 * @since JDK1.8
 */
public interface ResourceService {

  /**
   * @param key
   * @param value
   * @return
   */
  void setKey(String key, String value);

  Object getKey(String key);
}

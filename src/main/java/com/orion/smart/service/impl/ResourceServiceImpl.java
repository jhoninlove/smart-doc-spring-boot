package com.orion.smart.service.impl;

import com.orion.smart.service.ResourceService;
import java.util.concurrent.TimeUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

/**
 * @author <a href="http://github.com/athc">dujf</a>
 * @date 2018/9/10
 * @since JDK1.8
 */
@Service
public class ResourceServiceImpl implements ResourceService {

  /**
   * redisTemplate.opsForValue() 操作字符串
   * redisTemplate.opsForHash() 操作hash
   * redisTemplate.opsForList() 操作list
   * redisTemplate.opsForSet()  操作set
   * redisTemplate.opsForZSet() 操作有序set
   * redisTemplate.opsForGeo() 缓存地理位置信息
   * redisTemplate.opsForCluster()  redis Cluster集群时使用
   * redisTemplate.opsForHyperLogLog() redis存大数据  会调用压缩算法
   */
  private final RedisTemplate<String, String> redisTemplate;

  @Autowired public ResourceServiceImpl(RedisTemplate<String, String> redisTemplate) {this.redisTemplate = redisTemplate;}

  @Override
  public void setKey(String key, String value) {
    redisTemplate.opsForValue().set(key, value, 10, TimeUnit.MINUTES);
  }

  @Override public Object getKey(String key) {
    return redisTemplate.opsForValue().get(key);
  }
}

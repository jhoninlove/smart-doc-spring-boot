package com.orion.smart;

import com.power.doc.builder.ApiDocBuilder;
import com.power.doc.model.ApiConfig;
import java.util.ArrayList;
import org.junit.Test;

/**
 * @author <a href="http://github.com/athc">dujf</a>
 * @date 2018/8/31
 * @since JDK1.8
 */
public class ApiDocTest {

  /**
   * 测试生成文档
   */
  @Test
  public void testBuilderControllersApiSimple() {
    ApiConfig config = new ApiConfig();
    config.setServerUrl("http://localhost:8010");
    config.setAllInOne(true);
    config.isStrict();
    config.setOutPath("/Users/dujf/Downloads/md");
    ApiDocBuilder.builderControllersApi(config);
    //将生成的文档输出到d:\md目录下，严格模式下api-doc会检测Controller的接口注释
  }
}

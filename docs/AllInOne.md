# hello
## 创建
**URL:** http://localhost:8010/hello/

**Type:** post

**Content-Type:** application/x-www-form-urlencoded


**Request-parameters:**

Parameter | Type|Description|Required
---|---|---|---
id|int|id|false
account|string|账号|true
password|string|密码|false


**Request-example:**
```
smart-doc currently cannot provide examples of parameters for the RequestParam request mode.
```
**Response-fields:**

Field | Type|Description
---|---|---
id|int|id
account|string|账号
password|string|密码


**Response-example:**
```
{
	"id":931,
	"account":"ki5nxd",
	"password":"22ad55"
}
```

## 查询
**URL:** http://localhost:8010/hello/

**Type:** get

**Content-Type:** application/x-www-form-urlencoded


**Request-parameters:**

Parameter | Type|Description|Required
---|---|---|---
name|string|No comments found.|true


**Request-example:**
```
smart-doc currently cannot provide examples of parameters for the RequestParam request mode.
```
**Response-fields:**

Field | Type|Description
---|---|---
id|int|id
account|string|账号
password|string|密码


**Response-example:**
```
{
	"id":303,
	"account":"wsm356",
	"password":"8ay4qc"
}
```

# 
## 
**URL:** http://localhost:8010/redis/string/

**Type:** post

**Content-Type:** application/x-www-form-urlencoded



**Request-example:**
```
No request parameters are required.
```

**Response-example:**
```
this api return nothing.
```

## 
**URL:** http://localhost:8010/redis/string/

**Type:** get

**Content-Type:** application/x-www-form-urlencoded



**Request-example:**
```
No request parameters are required.
```

**Response-example:**
```
this api return nothing.
```

# 
## redis 操作list
**URL:** http://localhost:8010/redis/list/

**Type:** post

**Content-Type:** application/x-www-form-urlencoded



**Request-example:**
```
No request parameters are required.
```

**Response-example:**
```
this api return nothing.
```

## redis 操作list
**URL:** http://localhost:8010/redis/list/

**Type:** get

**Content-Type:** application/x-www-form-urlencoded



**Request-example:**
```
No request parameters are required.
```

**Response-example:**
```
this api return nothing.
```

# 
## 添加
**URL:** http://localhost:8010/redis/hash/

**Type:** post

**Content-Type:** application/x-www-form-urlencoded



**Request-example:**
```
No request parameters are required.
```

**Response-example:**
```
this api return nothing.
```

## 查询
**URL:** http://localhost:8010/redis/hash/

**Type:** get

**Content-Type:** application/x-www-form-urlencoded



**Request-example:**
```
No request parameters are required.
```

**Response-example:**
```
this api return nothing.
```

# 
## redis 操作list
**URL:** http://localhost:8010/redis/zset/

**Type:** post

**Content-Type:** application/x-www-form-urlencoded



**Request-example:**
```
No request parameters are required.
```

**Response-example:**
```
this api return nothing.
```

## redis 操作list
**URL:** http://localhost:8010/redis/zset/

**Type:** get

**Content-Type:** application/x-www-form-urlencoded



**Request-example:**
```
No request parameters are required.
```

**Response-example:**
```
this api return nothing.
```

# 资源
## 创建
**URL:** http://localhost:8010/re/

**Type:** post

**Content-Type:** application/x-www-form-urlencoded


**Request-parameters:**

Parameter | Type|Description|Required
---|---|---|---
name|string|用户名|true


**Request-example:**
```
smart-doc currently cannot provide examples of parameters for the RequestParam request mode.
```
**Response-fields:**

Field | Type|Description
---|---|---
no param name|string|The interface directly returns the string type value.


**Response-example:**
```
"n8olwl"
```

## 查询
**URL:** http://localhost:8010/re/

**Type:** get

**Content-Type:** application/x-www-form-urlencoded


**Request-parameters:**

Parameter | Type|Description|Required
---|---|---|---
name|string|No comments found.|true


**Request-example:**
```
smart-doc currently cannot provide examples of parameters for the RequestParam request mode.
```
**Response-fields:**

Field | Type|Description
---|---|---
no param name|string|The interface directly returns the string type value.


**Response-example:**
```
"3ubg2j"
```

# 
## 
**URL:** http://localhost:8010/grap/

**Type:** get

**Content-Type:** application/x-www-form-urlencoded



**Request-example:**
```
No request parameters are required.
```
**Response-fields:**

Field | Type|Description
---|---|---
name|string|No comments found.
description|string|No comments found.
fieldDefinitionsByName|map|No comments found.
└─any object|object|any object
interfaces|array|No comments found.
definition|object|No comments found.
└─sourceLocation|object|No comments found.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─line|int|No comments found.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─column|int|No comments found.
└─comments|array|No comments found.
└─name|string|No comments found.
└─implementz|array|No comments found.
└─directives|array|No comments found.
└─fieldDefinitions|array|No comments found.


**Response-example:**
```
{
	"name":"胤祥.冯",
	"description":"oiwu2s",
	"fieldDefinitionsByName":{
		"mapKey":{
			
		}
	},
	"interfaces":{
		"object":"any object"
	},
	"definition":{
		"sourceLocation":{
			"line":226,
			"column":296
		},
		"comments":{
			"object":"any object"
		},
		"name":"胤祥.冯",
		"implementz":{
			"object":"any object"
		},
		"directives":{
			"object":"any object"
		},
		"fieldDefinitions":{
			"object":"any object"
		}
	}
}
```

## 
**URL:** http://localhost:8010/grap/simple

**Type:** get

**Content-Type:** application/x-www-form-urlencoded



**Request-example:**
```
No request parameters are required.
```
**Response-fields:**

Field | Type|Description
---|---|---
name|string|No comments found.
description|string|No comments found.
fieldDefinitionsByName|map|No comments found.
└─any object|object|any object
interfaces|array|No comments found.
definition|object|No comments found.
└─sourceLocation|object|No comments found.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─line|int|No comments found.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─column|int|No comments found.
└─comments|array|No comments found.
└─name|string|No comments found.
└─implementz|array|No comments found.
└─directives|array|No comments found.
└─fieldDefinitions|array|No comments found.


**Response-example:**
```
{
	"name":"胤祥.冯",
	"description":"20hkwl",
	"fieldDefinitionsByName":{
		"mapKey":{
			
		}
	},
	"interfaces":{
		"object":"any object"
	},
	"definition":{
		"sourceLocation":{
			"line":904,
			"column":559
		},
		"comments":{
			"object":"any object"
		},
		"name":"胤祥.冯",
		"implementz":{
			"object":"any object"
		},
		"directives":{
			"object":"any object"
		},
		"fieldDefinitions":{
			"object":"any object"
		}
	}
}
```

## 
**URL:** http://localhost:8010/grap/union

**Type:** get

**Content-Type:** application/x-www-form-urlencoded



**Request-example:**
```
No request parameters are required.
```
**Response-fields:**

Field | Type|Description
---|---|---
name|string|No comments found.
description|string|No comments found.
types|array|No comments found.
typeResolver|object|No comments found.
definition|object|No comments found.
└─sourceLocation|object|No comments found.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─line|int|No comments found.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─column|int|No comments found.
└─comments|array|No comments found.
└─name|string|No comments found.
└─directives|array|No comments found.
└─memberTypes|array|No comments found.


**Response-example:**
```
{
	"name":"胤祥.冯",
	"description":"vqbnqw",
	"types":{
		"object":"any object"
	},
	"typeResolver":{
		
	},
	"definition":{
		"sourceLocation":{
			"line":96,
			"column":387
		},
		"comments":{
			"object":"any object"
		},
		"name":"胤祥.冯",
		"directives":{
			"object":"any object"
		},
		"memberTypes":{
			"object":"any object"
		}
	}
}
```

## 
**URL:** http://localhost:8010/grap/enum

**Type:** get

**Content-Type:** application/x-www-form-urlencoded



**Request-example:**
```
No request parameters are required.
```
**Response-fields:**

Field | Type|Description
---|---|---
name|string|No comments found.
description|string|No comments found.
valueDefinitionMap|map|No comments found.
└─any object|object|any object
definition|object|No comments found.
└─sourceLocation|object|No comments found.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─line|int|No comments found.
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;└─column|int|No comments found.
└─comments|array|No comments found.
└─name|string|No comments found.
└─enumValueDefinitions|array|No comments found.
└─directives|array|No comments found.
coercing|object|No comments found.


**Response-example:**
```
{
	"name":"胤祥.冯",
	"description":"xr84lf",
	"valueDefinitionMap":{
		"mapKey":{
			
		}
	},
	"definition":{
		"sourceLocation":{
			"line":738,
			"column":522
		},
		"comments":{
			"object":"any object"
		},
		"name":"胤祥.冯",
		"enumValueDefinitions":{
			"object":"any object"
		},
		"directives":{
			"object":"any object"
		}
	},
	"coercing":{
		
	}
}
```

# 
## 创建用户
**URL:** http://localhost:8010/graphql/user/

**Type:** post

**Content-Type:** application/json; charset=utf-8


**Request-parameters:**

Parameter | Type|Description|Required
---|---|---|---
account|string|账号|false
password|string|密码|false


**Request-example:**
```
{
	"account":"z4vktb",
	"password":"n7rkqs"
}
```
**Response-fields:**

Field | Type|Description
---|---|---
id|int|id
account|string|账号
password|string|密码


**Response-example:**
```
{
	"id":217,
	"account":"e65f6a",
	"password":"kmkcrm"
}
```

## user列表
**URL:** http://localhost:8010/graphql/user/

**Type:** get

**Content-Type:** application/x-www-form-urlencoded



**Request-example:**
```
No request parameters are required.
```
**Response-fields:**

Field | Type|Description
---|---|---
id|int|id
account|string|账号
password|string|密码


**Response-example:**
```
[
	{
		"id":148,
		"account":"ay1bn3",
		"password":"t5vies"
	}
]
```

## id 查询用户
**URL:** http://localhost:8010/graphql/user/{id}

**Type:** get

**Content-Type:** application/x-www-form-urlencoded


**Request-parameters:**

Parameter | Type|Description|Required
---|---|---|---
id|number|No comments found.|true


**Request-example:**
```
smart-doc currently cannot provide examples of parameters for the RequestParam request mode.
```
**Response-fields:**

Field | Type|Description
---|---|---
id|int|id
account|string|账号
password|string|密码


**Response-example:**
```
{
	"id":918,
	"account":"z744p8",
	"password":"d1x6e7"
}
```

## 用户总数
**URL:** http://localhost:8010/graphql/user/count

**Type:** get

**Content-Type:** application/x-www-form-urlencoded



**Request-example:**
```
No request parameters are required.
```
**Response-fields:**

Field | Type|Description
---|---|---
no param name|number|The interface directly returns the number type value.


**Response-example:**
```
998
```

# 
## redis 操作list
**URL:** http://localhost:8010/redis/set/

**Type:** post

**Content-Type:** application/x-www-form-urlencoded



**Request-example:**
```
No request parameters are required.
```

**Response-example:**
```
this api return nothing.
```

## redis 操作list
**URL:** http://localhost:8010/redis/set/

**Type:** get

**Content-Type:** application/x-www-form-urlencoded



**Request-example:**
```
No request parameters are required.
```

**Response-example:**
```
this api return nothing.
```

